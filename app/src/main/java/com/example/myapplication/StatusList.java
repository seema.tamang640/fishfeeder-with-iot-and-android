package com.example.myapplication;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class StatusList extends ArrayAdapter<Status> {

    private Activity context;
    List<Status> statusList;

    public StatusList(Activity context, List<Status> statusList){
        super(context, R.layout.list_layout, statusList);

        this.context =context;
        this.statusList= statusList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.list_layout, null, true);

        TextView textViewStatus = (TextView) listViewItem.findViewById(R.id.TextViewStatus);
        TextView textViewDate = (TextView) listViewItem.findViewById(R.id.TextViewDate);


        Status status =  statusList.get(position);

        textViewDate.setText(status.getSERVO_DATE());
   textViewStatus.setText(Integer.toString(status.getSERVO_STATUS()));


        return listViewItem;

    }
}
