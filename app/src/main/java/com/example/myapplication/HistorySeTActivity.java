package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HistorySeTActivity extends AppCompatActivity {

    DatabaseReference dreff;

    ListView listViewStatus1;

    List<TimeStatus> timeStatusList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_se_t);

        listViewStatus1 =(ListView)findViewById(R.id.listViewStatus1);


        timeStatusList = new ArrayList<>();


        dreff= FirebaseDatabase.getInstance().getReference("timestatus");


        dreff.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                timeStatusList.clear();

                for(DataSnapshot timestatusSnapshot: dataSnapshot.getChildren() ){
                    TimeStatus timestatus = timestatusSnapshot.getValue(TimeStatus.class);


                    timeStatusList.add(timestatus);

                }
                TimeStatusList adapter = new TimeStatusList(HistorySeTActivity.this,timeStatusList);

                if(adapter.getCount()!=0) {
                    listViewStatus1.setAdapter(adapter);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();



    }


}

