package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    FirebaseDatabase database;

    DatabaseReference databasestatus;
  DatabaseReference myRefData;
//    DatabaseReference myRefTime;
   TextView amount;
    TextView date;
    Button feednow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        databasestatus = FirebaseDatabase.getInstance().getReference("status");

        myRefData = FirebaseDatabase.getInstance().getReference("SERVO_STATUS");
        feednow =(Button) findViewById(R.id.FeedButton);
        feednow.setOnClickListener(this);

        amount = (TextView) findViewById(R.id.Amount);


        date=findViewById(R.id.date);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
        String currentDateandTime = sdf.format(new Date());

        date.setText(currentDateandTime);



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.FeedButton:
               // Toast.makeText(this, "Feed now clicked", Toast.LENGTH_SHORT).show();

//               myRefData = database.getReference("SERVO_STATUS");
                String data = amount.getText().toString();
                int data1 = Integer.parseInt(data);

                 myRefData.setValue(data1);


                String time = date.getText().toString();

                String id = databasestatus.push().getKey();

                Status status = new Status(id,data1, time);

                databasestatus.child(id).setValue(status);

                Toast.makeText(this, "Status updated", Toast.LENGTH_SHORT).show();

                finish();


                break;


        }
    }
}
