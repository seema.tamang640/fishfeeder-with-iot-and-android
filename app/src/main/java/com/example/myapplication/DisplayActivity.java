package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class DisplayActivity extends AppCompatActivity implements View.OnClickListener {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_display);

            Button feed = findViewById(R.id.buttonFish);
            Button setTime = findViewById(R.id.buttonClock);
            Button Logout = findViewById(R.id.buttonLog);

            Button History = findViewById(R.id.buttonHis);

            feed.setOnClickListener(this);
            setTime.setOnClickListener(this);
            Logout.setOnClickListener(this);
            History.setOnClickListener(this);

        }
        @Override
        public void onBackPressed(){
            //super.onBackPressed();

        }

        @Override
        public void onClick(View v) {
            switch(v.getId()){
                case R.id.buttonFish:
                    Toast.makeText(this, "Feed it clicked", Toast.LENGTH_SHORT).show();
                    Intent myIntent = new Intent(DisplayActivity.this, HomeActivity.class);
                    DisplayActivity.this.startActivity(myIntent);
                    break;

                case R.id.buttonClock:
                    Toast.makeText(this, "Settime now clicked", Toast.LENGTH_SHORT).show();
                    Intent myIntent1 = new Intent(DisplayActivity.this, SetTimeActivity.class);
                    DisplayActivity.this.startActivity(myIntent1);
                    break;


                case R.id.buttonLog:
                   // Toast.makeText(this, "Logout now clicked", Toast.LENGTH_SHORT).show();
                    Intent myIntent2 = new Intent(DisplayActivity.this, LoginActivity.class);
                    startActivity(myIntent2);

                    FirebaseAuth.getInstance().signOut();

                    finish();
                    break;

                case R.id.buttonHis:
                    Toast.makeText(this, "History now clicked", Toast.LENGTH_SHORT).show();
                    Intent myIntent3 = new Intent(DisplayActivity.this, HistoryActivity.class);
                    DisplayActivity.this.startActivity(myIntent3);
                    break;





            }

        }
    }
