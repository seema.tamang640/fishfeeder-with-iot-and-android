package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HistoryFeedActivity extends AppCompatActivity {

    DatabaseReference dref;


    ListView listViewStatus;
     List<Status> statusList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_feed);

        listViewStatus =(ListView)findViewById(R.id.listViewStatus);

        statusList =  new ArrayList<>();

        dref = FirebaseDatabase.getInstance().getReference("status");

        dref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                statusList.clear();

                for(DataSnapshot statusSnapshot: dataSnapshot.getChildren() ){
                    Status status = statusSnapshot.getValue(Status.class);


                    statusList.add(status);

                }
                StatusList adapter = new StatusList(HistoryFeedActivity.this,statusList);

                if(adapter.getCount()!=0) {
                    listViewStatus.setAdapter(adapter);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
