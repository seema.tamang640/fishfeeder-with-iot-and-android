package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SetTimeActivity extends AppCompatActivity implements View.OnClickListener {

    Button settime;
    FirebaseDatabase database;
    DatabaseReference databasestatus;
    DatabaseReference myRefData;
    DatabaseReference myRefDataTime;
    DatabaseReference myRefDataDate;
    EditText amount;
    EditText date;
    EditText datee;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settime);

        databasestatus = FirebaseDatabase.getInstance().getReference("timestatus");

        myRefData=FirebaseDatabase.getInstance().getReference("SERVO_NUMBER");
        myRefDataTime =FirebaseDatabase.getInstance().getReference("SERVO_TIME");
        myRefDataDate=FirebaseDatabase.getInstance().getReference("SERVO_DATE");

        settime = (Button) findViewById(R.id.FeedB);
        settime.setOnClickListener(this);

        amount = (EditText) findViewById(R.id.Amount1);
        date = (EditText) findViewById(R.id.Time);

        datee = (EditText) findViewById(R.id.FormatDate);


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.FeedB:
//                Toast.makeText(this, "Feed on time clicked", Toast.LENGTH_SHORT).show();
//
                String amt = amount.getText().toString();
                int num = Integer.parseInt(amt);

                myRefData.setValue(num);

                String time1 = date.getText().toString();

                myRefDataTime.setValue(time1);

                String dateformat = datee.getText().toString();

                myRefDataDate.setValue(dateformat);


                String id = databasestatus.push().getKey();

                TimeStatus status = new TimeStatus(id,num,time1,dateformat);

                databasestatus.child(id).setValue(status);

                Toast.makeText(this, "Status updated", Toast.LENGTH_SHORT).show();

                finish();

                break;
        }
    }
}
