package com.example.myapplication;

import java.util.Date;

public class Status {

    String SERVO_ID;
    int SERVO_STATUS;
    String SERVO_DATE;

    public Status(){

    }

    public Status(String SERVO_ID, int SERVO_STATUS, String SERVO_DATE) {
        this.SERVO_ID = SERVO_ID;
        this.SERVO_STATUS = SERVO_STATUS;
        this.SERVO_DATE = SERVO_DATE;
    }

    public String getSERVO_ID() {
        return SERVO_ID;
    }

    public void setSERVO_ID(String SERVO_ID) {
        this.SERVO_ID = SERVO_ID;
    }

    public int getSERVO_STATUS() {
        return SERVO_STATUS;
    }

    public void setSERVO_STATUS(int SERVO_STATUS) {
        this.SERVO_STATUS = SERVO_STATUS;
    }

    public String getSERVO_DATE() {
        return SERVO_DATE;
    }

    public void setSERVO_DATE(String SERVO_DATE) {
        this.SERVO_DATE = SERVO_DATE;
    }
}
