package com.example.myapplication;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.w3c.dom.Text;

import java.sql.Time;
import java.util.List;

public class TimeStatusList extends ArrayAdapter<TimeStatus> {

    private Activity context;
    List<TimeStatus> TimeStatusList;


    public TimeStatusList(Activity context, List<TimeStatus> TimeStatusList){

        super(context, R.layout.list_layout_time, TimeStatusList );

        this.context = context;
        this.TimeStatusList = TimeStatusList;
    }

    @NonNull
    @Override

    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItemm = inflater.inflate(R.layout.list_layout_time,null,true);

        TextView textViewStatus = (TextView) listViewItemm.findViewById(R.id.TextViewStatus1);
        TextView textViewDate = (TextView) listViewItemm.findViewById(R.id.TextViewDate1);
        TextView textViewDatee = (TextView) listViewItemm.findViewById(R.id.TextViewResult1);


        TimeStatus status = TimeStatusList.get(position);

        textViewDate.setText(status.getSERVO_TIME());
        textViewStatus.setText(Integer.toString(status.getSERVO_NUMBER()));
        textViewDatee.setText(status.getSERVO_DATE());

        return listViewItemm;

    }

}
