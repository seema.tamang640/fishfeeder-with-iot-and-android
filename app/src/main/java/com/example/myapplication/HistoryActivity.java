package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HistoryActivity extends AppCompatActivity {

    TextView SERVO;
    TextView TIME;
    DatabaseReference dref;
    DatabaseReference dreff;
    DatabaseError databaseError;
    String status1;
    String status2;
    Button button1;
    Button button2;

    ListView listViewStatus;
    ListView listViewStatus1;
    List<Status> statusList;
    List<TimeStatus> timeStatusList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        button1 = (Button)findViewById(R.id.buttonHistory1);
        button2 =(Button)findViewById(R.id.buttonHistory2);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(HistoryActivity.this, HistoryFeedActivity.class);
               startActivity(myIntent);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(HistoryActivity.this, HistorySeTActivity.class);
                startActivity(myIntent);
            }
        });


    }
}

