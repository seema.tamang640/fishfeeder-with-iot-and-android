package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.myapplication.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;


public class RegisterActivity extends AppCompatActivity{


    static int PReqCode =1 ;
    static int REQUESCODE =1;


    private EditText userEmail,userPassword,userPassword2,userName;
    private ProgressBar loadingProgress;
    private Button regBtn;


    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //inu views
        userEmail = findViewById(R.id.regMail);
        userPassword = findViewById(R.id.regPassword);
        userPassword2 = findViewById(R.id.regPassword2);
      //  userName = findViewById(R.id.regName);
        loadingProgress = findViewById(R.id.regProgressBar);
        regBtn = findViewById(R.id.regButton);
        loadingProgress.setVisibility(View.INVISIBLE);

        mAuth = FirebaseAuth.getInstance();

        regBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                regBtn.setVisibility(view.INVISIBLE);
                loadingProgress.setVisibility(view.VISIBLE);
                final String email = userEmail.getText().toString();
                final String password = userPassword.getText().toString();
                final String password2 = userPassword2.getText().toString();
              //  final String name = userName.getText().toString();


                if(email.isEmpty() || password.isEmpty()|| !password.equals(password2)){

                    //all fieds must be filled
                    //error message
                    showMessage("Please verify all fields");
                    regBtn.setVisibility(view.VISIBLE);
                    loadingProgress.setVisibility(view.INVISIBLE);

                }
                else{
                    //if everything is ok
                    //create useraacount method
                    createUserAccount(email,password);

                }

            }
        });


    }

    private void createUserAccount(String email,  String password) {

        //this method create user account with specific email and password
        mAuth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            //created successfully
                            showMessage("Account created");
                            //after we created user account we need to update his profie pic and name
                            updateUserInfo( mAuth.getCurrentUser());


                        }
                        else{
                            //account creation failed

                            showMessage("account creation failed" +task.getException().getMessage());
                            regBtn.setVisibility(View.VISIBLE);
                            loadingProgress.setVisibility(View.INVISIBLE);

                        }
                    }
                });



    }

    //update user photo and name
    private void updateUserInfo(final FirebaseUser currentUser) {

        showMessage("Register Complete");
        updateUI();

            }




    private void updateUI() {

        Intent displayActivity = new Intent(getApplicationContext(),DisplayActivity.class);
        startActivity(displayActivity);
        finish();

    }

    //simple method to show toast message
    private void showMessage(String message) {


        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();




    }


}
