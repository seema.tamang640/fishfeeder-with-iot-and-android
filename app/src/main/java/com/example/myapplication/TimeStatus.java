package com.example.myapplication;

public class TimeStatus {
    String SERVO_ID;
    int SERVO_NUMBER;
    String SERVO_TIME;
    String SERVO_DATE;

    public TimeStatus(){

    }

    public TimeStatus(String SERVO_ID, int SERVO_NUMBER, String SERVO_TIME, String SERVO_DATE) {
        this.SERVO_ID = SERVO_ID;
        this.SERVO_NUMBER = SERVO_NUMBER;
        this.SERVO_TIME = SERVO_TIME;
        this.SERVO_DATE = SERVO_DATE;
    }

    public String getSERVO_ID() {
        return SERVO_ID;
    }

    public void setSERVO_ID(String SERVO_ID) {
        this.SERVO_ID = SERVO_ID;
    }

    public int getSERVO_NUMBER() {
        return SERVO_NUMBER;
    }

    public void setSERVO_NUMBER(int SERVO_NUMBER) {
        this.SERVO_NUMBER = SERVO_NUMBER;
    }

    public String getSERVO_TIME() {
        return SERVO_TIME;
    }

    public void setSERVO_TIME(String SERVO_TIME) {
        this.SERVO_TIME = SERVO_TIME;
    }

    public String getSERVO_DATE() {
        return SERVO_DATE;
    }

    public void setSERVO_DATE(String SERVO_DATE) {
        this.SERVO_DATE = SERVO_DATE;
    }
}
